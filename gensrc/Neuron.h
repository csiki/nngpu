
//  @ Project : NNGPU
//  @ File Name : Neuron.h
//  @ Author : Viktor T�th
//
//


#if !defined(_NEURON_H)
#define _NEURON_H

#include "DeviceEntity.h"
//#include "Network.h"

class Network;

class Neuron : public DeviceEntity
{
public:
	Neuron(unsigned int id, unsigned int nout);
	~Neuron();

	bool activate();
	bool update();
	void forwardprop();
	void backprop();
	void input(unsigned int dendriteId, double val);
	void connectInputs(const Neuron* axons);
	void connectOutput(unsigned int outputId, unsigned int dentriteId);
	void assignWeights(const double* weights);
	void backpropError(double weightedError);
	unsigned int getId() const;
	
	// Inherited from DeviceEntity
	void copyToDevice(void** dest);
	void copyToHost(void** dest);
	
private:
	const unsigned int id_;
	unsigned int nerror_;
	unsigned int nin_;
	unsigned int ninputsAssigned_;
	double biasweight_;
	double* weights_;
	double* inputs_;
	double* errors_;
	double activation_;
	unsigned int* inputIds_;
	unsigned int* outputIds_;
	unsigned int* outputDendriteIds_;
	Network *network;
};

#endif  //_NEURON_H
