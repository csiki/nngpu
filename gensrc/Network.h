
//  @ Project : NNGPU
//  @ File Name : Network.h
//  @ Author : Viktor T�th
//
//


#if !defined(_NETWORK_H)
#define _NETWORK_H

#include <string>
#include "DeviceEntity.h"
#include "Statistics.h"
#include "Neuron.h"
#include "NeuronType.h"

class Network : public DeviceEntity
{
public:
	static void initGPU();

	Network();
	~Network();

	bool init(std::string xmlPath);
	void init(const int* layers);
	Neuron* accessNeuron(unsigned int id);
	Statistics train(const double** const trainData, const double** const testData);
	void run(const double* input, double** output); // approximate / classify
	void setLearningRate(double learningRate);
	double getLearningRate();
	bool save(std::string xmlPath);
	
	// Inherited from DeviceEntity
	void copyToDevice(void** dest);
	void copyToHost(void** dest);
	
private:
	// GPU specific fields
	static unsigned int nblocksmax_;
	static unsigned int nthreadsmax_;
	
	// Fields
	NeuronType type;
	unsigned int nin_;
	unsigned int nout_;
	unsigned int nround_;
	double learningRate_;
	Neuron* neurons; // 1D array
	Neuron** inputNeurons; // 1D array of Neuron pointers
	Neuron** outputNeurons; // 1D array of Neuron pointers
	unsigned int** neronsPerRound_; // nround_ piece of id lists
	
	void initRounds();
};

#endif  //_NETWORK_H
