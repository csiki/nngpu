
//  @ Project : NNGPU
//  @ File Name : Network.cpp
//  @ Author : Viktor T�th
//
//


#include "Network.h"
#include "Statistics.h"

// static
unsigned int Network::nblocksmax_ = 0;
unsigned int Network::nthreadsmax_ = 0;

void Network::initGPU()
{

}

Network::Network()
{

}

Network::~Network()
{

}

bool Network::init(std::string xmlPath)
{
	return false;
}

void Network::init(const int* layers)
{

}

Neuron* Network::accessNeuron(unsigned int id)
{
	return nullptr;
}

Statistics Network::train(const double** const trainData, const double** const testData)
{
	return Statistics();
}

void Network::run(const double* input, double** output)
{

}

void Network::setLearningRate(double lrate)
{

}

double Network::getLearningRate()
{
	return .0;
}

void Network::copyToDevice(void** dest)
{

}

void Network::copyToHost(void** dest)
{

}

void Network::initRounds()
{

}

