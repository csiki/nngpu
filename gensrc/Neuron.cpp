
//  @ Project : NNGPU
//  @ File Name : Neuron.cpp
//  @ Author : Viktor T�th
//
//


#include "Neuron.h"

Neuron::Neuron(unsigned int id, unsigned int nout)
	: id_(id), nerror_(nout)
{
	// allocate for outputIds_, outputDendriteIds_, errors_
}

Neuron::~Neuron()
{

}

bool Neuron::activate()
{
	return false;
}

bool Neuron::update()
{
	return false;
}

void Neuron::forwardprop()
{

}

void Neuron::backprop()
{

}

void Neuron::input(unsigned int dendriteId, double val)
{

}

void Neuron::connectInputs(const Neuron* axons)
{

}

void Neuron::connectOutput(unsigned int outputId, unsigned int dentriteId)
{

}

void Neuron::backpropError(double weightedError)
{

}

unsigned int Neuron::getId() const
{
	return 0;
}

void Neuron::assignWeights(const double* weights)
{

}

void Neuron::copyToDevice(void** dest)
{

}

void Neuron::copyToHost(void** dest)
{

}

