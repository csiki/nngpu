
//  @ Project : NNGPU
//  @ File Name : Neuron.h
//  @ Author : Viktor T�th
//
//


#if !defined(_NEURON_H)
#define _NEURON_H

#include "DeviceEntity.h"
#include "NeuronType.h"

/*
	Sigmoid function declaration.
*/
double sigmoid(double x);

class Network;

class Neuron : public DeviceEntity
{
public:
	Neuron(unsigned int id = 0, unsigned int nout = 0, NeuronType type = NeuronType::HIDDEN, Network* network = nullptr);
	~Neuron();

	bool activate();
	bool update();
	void forwardprop();
	void backprop();
	void input(unsigned int dendriteId, double val);
	void connectInputs(Neuron** axons, const double* weights, unsigned int nin);
	void backpropError(double weightedError);
	unsigned int getId() const;
	void setId(unsigned int id);
	void setType(NeuronType type);
	void setNetwork(Network* network);
	void setBiasweight(double biasweight);
	void initOutput(unsigned int nout);
	double getInput(unsigned int index) const;
	
	// Inherited from DeviceEntity
	void copyToDevice(void** dest);
	void copyToHost(void** dest);
	
private:
	// can be only called by another Neuron
	void connectOutput(unsigned int outputId, unsigned int dentriteId);

	unsigned int id_;
	unsigned int nout_;
	unsigned int nin_;
	unsigned int ninputsAssigned_;
	unsigned int nerrorsAssigned_;
	double biasweight_;
	double* weights_;
	double* inputs_;
	double* errors_;
	double activation_;
	unsigned int* inputIds_;
	unsigned int* outputIds_;
	unsigned int* outputDendriteIds_;
	Network *network_;
	NeuronType type_;
};

#endif  //_NEURON_H
