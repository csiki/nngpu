
//  @ Project : NNGPU
//  @ File Name : main.cpp
//  @ Author : Viktor T�th
//
//

#include <stdlib.h>
#include <time.h>
#include "Neuron.h"

int main(int argc, char* argv[])
{
	srand( static_cast<unsigned int> (time(NULL)) );

	Neuron n(1, 4);

	return 0;
}