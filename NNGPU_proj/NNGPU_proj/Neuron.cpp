
//  @ Project : NNGPU
//  @ File Name : Neuron.cpp
//  @ Author : Viktor T�th
//
//

#include <math.h>
#include <stdlib.h>
#include <stdexcept>
#include "Neuron.h"

/*
	Sigmoid function definition.
*/
double sigmoid(double x)
{
	return 2.0 / (1.0 + exp(-2.0 * x)) - 1.0;
}

/*Neuron::Neuron()
{
	id_ = 0;
	nout_ = 0;
	type_ = NeuronType::HIDDEN;

}*/

Neuron::Neuron(unsigned int id, unsigned int nout, NeuronType type, Network* network)
	: id_(id), nout_(nout), type_(type), network_(network)
{
	nin_ = 0;
	ninputsAssigned_ = 0;
	nerrorsAssigned_ = 0;
	biasweight_ = static_cast<double> (rand()) / static_cast<double> (RAND_MAX) + 0.5;
	activation_ = .0;

	weights_ = nullptr;
	inputs_ = nullptr;
	inputIds_ = nullptr;

	errors_ = nullptr;
	outputIds_ = nullptr;
	outputDendriteIds_ = nullptr;

	if (nout_ > 0)
	{
		errors_ = new double [nout_];
		outputIds_ = new unsigned int [nout_];
		outputDendriteIds_ = new unsigned int [nout_];

		for (unsigned int i = 0; i < nout; ++i)
		{
			errors_[i] = .0;
			outputIds_[i] = 0;
			outputDendriteIds_[i] = 0;
		}
	}
}

Neuron::~Neuron()
{
	delete [] weights_;
	delete [] inputs_;
	delete [] inputIds_;
	delete [] errors_;
	delete [] outputIds_;
	delete [] outputDendriteIds_;
}

bool Neuron::activate()
{
	return false;
}

bool Neuron::update()
{
	return false;
}

void Neuron::forwardprop()
{

}

void Neuron::backprop()
{

}

void Neuron::input(unsigned int dendriteId, double val)
{
	if (dendriteId > nin_ || dendriteId < 0)
		throw std::out_of_range("Out of range: Neuron::input dendriteId");

	inputs_[dendriteId] = val;
	++ninputsAssigned_;
}

void Neuron::connectInputs(Neuron** axons, const double* weights, unsigned int nin)
{
	if (this->type_ == NeuronType::INPUT)
	{
		nin_ = 1;
		inputs_ = new double;
	}
	else // hidden or output
	{
		nin_ = nin;
		inputs_ = new double [nin_];
		inputIds_ = new unsigned int [nin_];
		weights_ = new double[nin_];
	
		for (unsigned int i = 0; i < nin_; ++i)
		{
			inputs_[i] = .0;
			inputIds_[i] = axons[i]->getId();
			weights_[i] = weights[i];
			axons[i]->connectOutput(id_, i); // i as dendrite id
		}
	}
}

void Neuron::connectOutput(unsigned int outputId, unsigned int dentriteId)
{

}

void Neuron::backpropError(double weightedError)
{
	if (nerrorsAssigned_ >= nout_)
		throw std::out_of_range("Out of range: Neuron::backpropError - too much error assigned");

	errors_[nerrorsAssigned_++] = weightedError;
}

unsigned int Neuron::getId() const
{
	return id_;
}

void Neuron::setId(unsigned int id)
{
	id_ = id;
}

void Neuron::setType(NeuronType type)
{
	type_ = type;
}

void Neuron::setNetwork(Network* network)
{
	network_ = network;
}

void Neuron::setBiasweight(double biasweight)
{
	biasweight_ = biasweight;
}

void Neuron::initOutput(unsigned int nout)
{
	nout_ = nout;

	if (nout_ > 0)
	{
		errors_ = new double [nout_];
		outputIds_ = new unsigned int [nout_];
		outputDendriteIds_ = new unsigned int [nout_];

		for (unsigned int i = 0; i < nout; ++i)
		{
			errors_[i] = .0;
			outputIds_[i] = 0;
			outputDendriteIds_[i] = 0;
		}
	}
}

void Neuron::copyToDevice(void** dest)
{

}

void Neuron::copyToHost(void** dest)
{

}

double Neuron::getInput(unsigned int index) const
{
	if (index >= nin_ || index < 0)
		throw std::out_of_range("Out of range: Neuron::getInput index value");

	return inputs_[index];
}