
//  @ Project : NNGPU
//  @ File Name : Statistics.h
//  @ Author : Viktor T�th
//
//


#if !defined(_STATISTICS_H)
#define _STATISTICS_H

#include <string>

class Statistics
{
public:
	Statistics(unsigned int maxnstat = 1);
	~Statistics();

	void addStat(std::string name, double val);
	double getStat(std::string name);
	std::string toString();
	
private:
	unsigned int nstat_;
	unsigned int maxnstat_;
	std::string* statnames_;
	double* statvals_;
};

#endif  //_STATISTICS_H
