
//  @ Project : NNGPU
//  @ File Name : Network.cpp
//  @ Author : Viktor T�th
//
//


#include "Network.h"
#include "Statistics.h"
#include "pugixml.hpp"
#include <stdexcept>

// static
unsigned int Network::nblocksmax_ = 0;
unsigned int Network::nthreadsmax_ = 0;

void Network::initGPU()
{

}

Network::Network()
{
	nneuron_ = 0;
	nin_ = 0;
	nout_ = 0;
	nround_ = 0;
	nperround_ = 0;
	learningRate_ = 0.5; // default value
	neurons_ = nullptr;
	inputNeurons_ = nullptr;
	outputNeurons_ = nullptr;
	neronsPerRound_ = nullptr;
}

Network::~Network()
{
	delete [] neurons_;
	delete [] inputNeurons_;
	delete [] outputNeurons_;
	
	for (unsigned int i = 0; i < nround_; ++i)
		delete [] neronsPerRound_[i];
	
	delete [] neronsPerRound_;
}

bool Network::init(std::string xmlPath)
{
	pugi::xml_document doc;
	pugi::xml_parse_result network = doc.load_file(xmlPath.c_str());

	if (network)
	{
		pugi::xpath_node networkNode = doc.select_single_node("/network");

		// count neurons
		for (pugi::xml_node neuronNode = networkNode.node().child("neuron"); neuronNode; neuronNode = neuronNode.next_sibling("neuron"))
		{
			++nneuron_;

			if ( std::strcmp(neuronNode.attribute("type").as_string(), "input") == 0)
				++nin_;
			else if ( std::strcmp(neuronNode.attribute("type").as_string(), "output") == 0)
				++nout_;
		}

		// allocate
		neurons_ = new Neuron[nneuron_];
		inputNeurons_ = new Neuron*[nin_];
		outputNeurons_ = new Neuron*[nout_];

		// set neuron attributes
		unsigned int index = 0;
		unsigned int inputIndex = 0;
		unsigned int outputIndex = 0;
		for (pugi::xml_node neuronNode = networkNode.node().child("neuron"); neuronNode; neuronNode = neuronNode.next_sibling("neuron"))
		{
			neurons_[index].setId( neuronNode.child("id").text().as_uint() );
			neurons_[index].setNetwork(this);

			// biasweight
			if (neuronNode.child("biasweight"))
				neurons_[index].setBiasweight( neuronNode.child("biasweight").text().as_double() );

			// if input
			if ( neuronNode.attribute("type") && std::strcmp(neuronNode.attribute("type").as_string(), "input") == 0)
			{
				neurons_[index].setType(NeuronType::INPUT);
				inputNeurons_[inputIndex] = &neurons_[index];
				neurons_[index].initOutput( neuronNode.child("outputnum").text().as_uint() );
				neurons_[index].connectInputs(nullptr, nullptr, 1); // connect only one input
				++inputIndex;
			}
			// if output
			else if ( neuronNode.attribute("type") && std::strcmp(neuronNode.attribute("type").as_string(), "output") == 0)
			{
				neurons_[index].setType(NeuronType::OUTPUT);
				outputNeurons_[inputIndex] = &neurons_[index];
				// no output - so no outputnum
				++outputIndex;
			}
			// if hidden
			else
			{
				neurons_[index].setType(NeuronType::HIDDEN);
				neurons_[index].initOutput( neuronNode.child("outputnum").text().as_uint() );
			}
			
			++index;
		}

		// connect inputs
		index = 0;
		for (pugi::xml_node neuronNode = networkNode.node().child("neuron"); neuronNode; neuronNode = neuronNode.next_sibling("neuron"))
		{
			// gather inputs
			if (neuronNode.child("inputs"))
			{
				unsigned int neuronInputNum = 0;
				for (pugi::xml_node inputNeuronNode = neuronNode.child("inputs").child("neuron"); inputNeuronNode; inputNeuronNode = inputNeuronNode.next_sibling("neuron"))
					++neuronInputNum;

				// allocate for input neurons
				Neuron** neuronInputs = new Neuron*[neuronInputNum];
				double* neuronWeights = new double[neuronInputNum];

				unsigned int inputNeuronIndex = 0;
				for (pugi::xml_node inputNeuronNode = neuronNode.child("inputs").child("neuron"); inputNeuronNode; inputNeuronNode = inputNeuronNode.next_sibling("neuron"))
				{
					neuronInputs[inputNeuronIndex] = &neurons_[ inputNeuronNode.child("id").text().as_uint() ];
					if (inputNeuronNode.child("weight"))
						neuronWeights[inputNeuronIndex] = inputNeuronNode.child("weight").text().as_double();
					else
						neuronWeights[inputNeuronIndex] = static_cast<double> (rand()) / static_cast<double> (RAND_MAX) + 0.5;

					++inputNeuronIndex;
				}

				neurons_[index].connectInputs(neuronInputs, neuronWeights, neuronInputNum);

				delete [] neuronInputs;
				delete [] neuronWeights;
			}

			++index;
		}

		return true;
	}

	return false;
}

void Network::init(const int* layers, unsigned int nlayer)
{
	/*if (nlayer < 2)
		throw std::runtime_error("Two layers required at least!");

	// count
	nin_ = layers[0];
	nout_ = layers[nlayer - 1];
	for (unsigned int i = 0; i < nlayer; ++i)
	{
		nneuron_ += layers[i];
	}

	// allocate
	neurons_ = new Neuron[nneuron_];
	inputNeurons_ = new Neuron*[nin_];
	outputNeurons_ = new Neuron*[nout_];

	unsigned int current_id = 0;

	// input layer
	for (unsigned int i = 0; i < layers[0]; ++i)
	{
		neurons_[current_id].setId(current_id);
		inputNeurons_[i] = &neurons_[current_id];
		++current_id;
	}

	// output layer
	// hidden layers*/
}

Neuron* Network::accessNeuron(unsigned int id)
{
	if (id >= nneuron_ || id < 0)
		throw std::out_of_range("Out of range: Network::accessNeuron id");

	return &neurons_[id];
}

Statistics Network::train(const double** const trainData, const double** const testData)
{
	return Statistics();
}

void Network::run(const double* input, double** output)
{

}

void Network::setLearningRate(double lrate)
{
	learningRate_ = lrate;
}

double Network::getLearningRate()
{
	return learningRate_;
}

void Network::copyToDevice(void** dest)
{

}

void Network::copyToHost(void** dest)
{

}

void Network::initRounds()
{

}

