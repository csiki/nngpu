
//  @ Project : NNGPU
//  @ File Name : NeuronType.h
//  @ Author : Viktor Tóth
//
//


#if !defined(_NEURONTYPE_H)
#define _NEURONTYPE_H


enum NeuronType
{
	HIDDEN,
	INPUT,
	OUTPUT
};

#endif  //_NEURONTYPE_H
