
//  @ Project : NNGPU
//  @ File Name : DeviceEntity.h
//  @ Author : Viktor T�th
//
//


#if !defined(_DEVICEENTITY_H)
#define _DEVICEENTITY_H


class DeviceEntity
{
public:
	virtual void copyToDevice(void** dest) = 0;
	virtual void copyToHost(void** dest) = 0;
};

#endif  //_DEVICEENTITY_H
